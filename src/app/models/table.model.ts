export class Table {
    constructor(
      public id: number,
      public name: string,
      public numberOfPax: number,
      public status: string
    ) {}
  }
  