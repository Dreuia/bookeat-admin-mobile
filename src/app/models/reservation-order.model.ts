export class ReservationOrder {
    constructor(
      public id: number,
      public orderRefNo: string,
      public timeSlot: string,
      public peopleCount: number,
      public occasion: string,
      public status: string,
      public reservedBy: string,
      public tableId: number
    ) {}
  }
  