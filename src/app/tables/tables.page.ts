import { Component, OnInit } from '@angular/core';
import { Table } from '../models/table.model';
import { TableService } from '../services/table.service';

@Component({
  selector: 'app-tables',
  templateUrl: './tables.page.html',
  styleUrls: ['./tables.page.scss'],
})
export class TablesPage implements OnInit {
  tables: Table[];
  isSearching = false;
  searchTerm = '';

  constructor(private tablesService: TableService) { }

  ngOnInit() {
    this.tables = this.tablesService.tables;
  }

  searchTable() {
    this.isSearching = true;
    this.requeryTables();
  }

  requeryTables() {
    this.tables = this.tablesService.tables.filter(
      (table: Table) => {
        this.isSearching = false;
        return table.name.toLowerCase().includes(this.searchTerm.toLowerCase());
      }
    );
  }

}
