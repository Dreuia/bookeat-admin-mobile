import { Component, OnInit } from "@angular/core";
import { NavController, ToastController } from "@ionic/angular";
import { AuthService } from "../services/auth.service";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.page.html",
  styleUrls: ["./profile.page.scss"],
})
export class ProfilePage implements OnInit {
  constructor(
    private authService: AuthService,
    private navCtrl: NavController,
    private toastController: ToastController
  ) {}

  ngOnInit() {}

  async logout() {
    const toast = await this.toastController.create({
      message: "Success",
      duration: 2000,
    });
    toast.present();

    this.authService.logout();
    this.navCtrl.navigateRoot("/auth");
  }
}
