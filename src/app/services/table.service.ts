import { Injectable } from "@angular/core";
import { Table } from "../models/table.model";

@Injectable({
  providedIn: "root",
})
export class TableService {
  private _tables: Table[] = [
    new Table(1, "Meja 1", 4, "Reserved"),
    new Table(2, "Meja 2", 6, "Unavailable"),
    new Table(3, "Meja 3", 8, "Available")
  ];

  get tables() {
    return [...this._tables];
  }

  constructor() {}

  getTable(id: number) {
    return {...this._tables.find(x => x.id === id)};
  }
}
