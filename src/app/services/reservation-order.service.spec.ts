import { TestBed } from '@angular/core/testing';

import { ReservationOrderService } from './reservation-order.service';

describe('ReservationOrderService', () => {
  let service: ReservationOrderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReservationOrderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
