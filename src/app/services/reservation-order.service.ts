import { Injectable } from '@angular/core';
import { ReservationOrder } from '../models/reservation-order.model';

@Injectable({
  providedIn: 'root'
})
export class ReservationOrderService {
  private _inProgressOrders: ReservationOrder[] = [
    new ReservationOrder(1, "R001", "13:00 - 15:00", 4, "Business", "In-progress", "Anton", null),
    new ReservationOrder(2, "R002", "20:00 - 22:00", 2, "Night Date", "In-progress", "Andre", null)
  ];

  private _completedOrders: ReservationOrder[] = [
    new ReservationOrder(3, "R009", "20:00 - 22:00", 2, "Night Date", "Completed", "Angel", 1)
  ];

  get inProgressOrders() {
    return [...this._inProgressOrders];
  }

  constructor() { }

  getReservationOrderByTable(tableId: number) {
    return {...this._completedOrders.find(x => x.tableId === tableId)};
  }
}
