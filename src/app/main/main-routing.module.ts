import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { MainPage } from "./main.page";

const routes: Routes = [
  {
    path: "tabs",
    component: MainPage,
    children: [
      {
        path: "tables",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../tables/tables.module").then((m) => m.TablesPageModule),
          },
          {
            path: ":tableId",
            loadChildren: () =>
              import("../table-detail/table-detail.module").then(
                (m) => m.TableDetailPageModule
              ),
          },
        ],
      },
      {
        path: 'profile',
        children: [
          {
            path: '',
            loadChildren: () => import('../profile/profile-routing.module').then(m => m.ProfilePageRoutingModule)
          }
        ]
      },
      {
        path: "",
        redirectTo: "/main/tabs/tables",
        pathMatch: "full",
      },
    ],
  },
  {
    path: "",
    redirectTo: "/main/tabs/tables",
    pathMatch: "full",
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainPageRoutingModule {}
