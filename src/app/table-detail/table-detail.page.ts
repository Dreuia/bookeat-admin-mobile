import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import {
  ModalController,
  NavController,
  ToastController,
} from "@ionic/angular";
import { CreateReservationComponent } from "../components/create-reservation/create-reservation.component";
import { SelectReservationComponent } from "../components/select-reservation/select-reservation.component";
import { ReservationOrder } from "../models/reservation-order.model";
import { Table } from "../models/table.model";
import { ReservationOrderService } from "../services/reservation-order.service";
import { TableService } from "../services/table.service";

@Component({
  selector: "app-table-detail",
  templateUrl: "./table-detail.page.html",
  styleUrls: ["./table-detail.page.scss"],
})
export class TableDetailPage implements OnInit {
  table: Table;
  isSearching = false;
  searchTerm = '';

  // available's table properties
  inProgressReservationOrders: ReservationOrder[];

  // reserved's table properties
  tableReservationOrder: ReservationOrder;

  constructor(
    private route: ActivatedRoute,
    private navCtrl: NavController,
    private tableService: TableService,
    private reservationOrderService: ReservationOrderService,
    private modalCtrl: ModalController,
    private toastController: ToastController
  ) {}

  ngOnInit() {
    // get table properties from url path
    this.route.paramMap.subscribe((paramMap) => {
      if (!paramMap.has("tableId")) {
        this.navCtrl.navigateBack("/main/tabs/tables");
        return;
      }
      this.table = this.tableService.getTable(+paramMap.get("tableId"));
    });

    // for available's table
    if (this.table.status === "Available") {
      this.inProgressReservationOrders = this.reservationOrderService.inProgressOrders;
    }

    // for reserved's table
    if (this.table.status === "Reserved") {
      this.tableReservationOrder = this.reservationOrderService.getReservationOrderByTable(
        this.table.id
      );
    }
  }

  searchReservation() {
    this.isSearching = true;
    this.requeryReservations();
  }

  requeryReservations() {
    this.inProgressReservationOrders = this.reservationOrderService.inProgressOrders.filter(
      (reservationOrder: ReservationOrder) => {
        this.isSearching = false;
        return reservationOrder.orderRefNo.toLowerCase().includes(this.searchTerm.toLowerCase());
      }
    );
  }

  createAndSelectReservation() {
    this.modalCtrl
      .create({
        component: CreateReservationComponent
      })
      .then((modalEl) => {
        modalEl.present();
        return modalEl.onDidDismiss();
      })
      .then(async (resultData) => {
        if (resultData.role === "confirm") {
          const toast = await this.toastController.create({
            message: "Success",
            duration: 2000,
          });
          toast.present();

          this.navCtrl.navigateBack("/main/tabs/tables");
        }
      });
  }

  onSelectReservation(selectedReservationOrder: ReservationOrder) {
    this.modalCtrl
      .create({
        component: SelectReservationComponent,
        componentProps: { selectedReservationOrder: selectedReservationOrder },
      })
      .then((modalEl) => {
        modalEl.present();
        return modalEl.onDidDismiss();
      })
      .then(async (resultData) => {
        if (resultData.role === "confirm") {
          const toast = await this.toastController.create({
            message: "Success",
            duration: 2000,
          });
          toast.present();

          this.navCtrl.navigateBack("/main/tabs/tables");
        }
      });
  }

  async setToUnavailable() {
    const toast = await this.toastController.create({
      message: "Success",
      duration: 2000,
    });
    toast.present();

    this.navCtrl.navigateBack("/main/tabs/tables");
  }

  async setToAvailable() {
    const toast = await this.toastController.create({
      message: "Success",
      duration: 2000,
    });
    toast.present();

    this.navCtrl.navigateBack("/main/tabs/tables");
  }
}
