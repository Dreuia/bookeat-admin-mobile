import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ReservationOrder } from 'src/app/models/reservation-order.model';

@Component({
  selector: 'app-select-reservation',
  templateUrl: './select-reservation.component.html',
  styleUrls: ['./select-reservation.component.scss'],
})
export class SelectReservationComponent implements OnInit {
  @Input() selectedReservationOrder: ReservationOrder;

  constructor(private modalCtrl: ModalController) {}

  ngOnInit() {}

  onCancel() {
    this.modalCtrl.dismiss(null, 'cancel');
  }

  onSelect() {
    this.modalCtrl.dismiss({ message: 'Select reservation success!' }, 'confirm');
  }
}
